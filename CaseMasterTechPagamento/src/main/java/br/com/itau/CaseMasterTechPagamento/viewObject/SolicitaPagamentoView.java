package br.com.itau.CaseMasterTechPagamento.viewObject;

public class SolicitaPagamentoView {
	private int idPedido;

	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
}
