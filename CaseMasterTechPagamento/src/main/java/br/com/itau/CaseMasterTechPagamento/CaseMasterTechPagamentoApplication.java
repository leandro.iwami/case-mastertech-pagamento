package br.com.itau.CaseMasterTechPagamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaseMasterTechPagamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaseMasterTechPagamentoApplication.class, args);
	}

}
