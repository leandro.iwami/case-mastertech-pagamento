package br.com.itau.CaseMasterTechPagamento.listenner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.itau.CaseMasterTechPagamento.services.PagamentoService;
import br.com.itau.CaseMasterTechPagamento.viewObject.SolicitaPagamentoView;

@Component
public class PagamentoListenner {
	
	@Autowired
	PagamentoService pagamentoService;
	
	@Bean
	public RecordMessageConverter converter() {
		return new StringJsonMessageConverter();
	}


	@KafkaListener(id="consumer", topics="efetivapagamento")
	public void consumir(@Payload SolicitaPagamentoView solicitaPagamento) {
		pagamentoService.pagarPedido(solicitaPagamento);
	}

}
