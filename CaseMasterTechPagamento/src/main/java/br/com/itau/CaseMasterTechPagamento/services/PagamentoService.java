package br.com.itau.CaseMasterTechPagamento.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.CaseMasterTechPagamento.producer.PagamentoProducer;
import br.com.itau.CaseMasterTechPagamento.viewObject.RespostaPagamentoView;
import br.com.itau.CaseMasterTechPagamento.viewObject.SolicitaPagamentoView;

@Service
public class PagamentoService {
	
	@Autowired
	PagamentoProducer pagamentoProducer;
	
	public void pagarPedido(SolicitaPagamentoView solicitaPagamento) {
		double numero = Math.random();
		RespostaPagamentoView respostaPagamentoView = new RespostaPagamentoView();
		respostaPagamentoView.setIdPedido(solicitaPagamento.getIdPedido());
		if (numero < 0.5) {
			respostaPagamentoView.setPagamentoEfetivado(false);
			pagamentoProducer.publicar(respostaPagamentoView);
		}else {
			respostaPagamentoView.setPagamentoEfetivado(true);
			pagamentoProducer.publicar(respostaPagamentoView);
		}
	}
}
