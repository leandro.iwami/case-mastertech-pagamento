package br.com.itau.CaseMasterTechPagamento.viewObject;

public class RespostaPagamentoView {
	
	private int idPedido;
	private boolean pagamentoEfetivado;
	
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public boolean isPagamentoEfetivado() {
		return pagamentoEfetivado;
	}
	public void setPagamentoEfetivado(boolean pagamentoEfetivado) {
		this.pagamentoEfetivado = pagamentoEfetivado;
	}
	
	
}
