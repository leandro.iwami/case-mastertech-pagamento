package br.com.itau.CaseMasterTechPagamento.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.com.itau.CaseMasterTechPagamento.viewObject.RespostaPagamentoView;

@Service
public class PagamentoProducer {

	@Autowired
	KafkaTemplate<Object, Object> kafkaTemplate;
	
	public void publicar(RespostaPagamentoView resposta) {
		kafkaTemplate.send("respostapagamento", resposta);
	}


}
